### :blue[The Holdius Eleven System]
:divider[Lore Dump]

*In the Galactic South of the Gothic Sector, right next to the Great Rift is a small system called Holdius Eleven. A red Dwarf with 11 more or less notable celestial bodies orbiting it, a strange rift has opened. The Imperials stranded from terra has sent out a request from help, but did it arrive in time?*

>##### Holdius Volcanus
> ---
> A scolding hot, volcanic planet with a few settlements. Mainly research facilities, but a few industries gathering the energy from the molten core of the planet and the heat of the red dwarf. 

>##### Mirrorius
> ---
> A barren desert planet, with lakes made of liquid mercury. Extremely toxic and hostile environment, but valuable for its rare metal and resources. 

> ##### Jupura
> ---
> A planet with a constant hurricane blowing all around it - making it extremely difficult landing and taking off in spaceships. 3 major space elevators with attached space stations are used to transport resources and people on and off the planet. It's mainly a craft world making small arms with its facilities under ground. 

> ##### Colenfar
> ---
> An ocean planet with no notable resources or population

> ##### Blue H-1
> ---
> A planet with a frozen core and a snow-covered landscape. It is mainly used for harvesting ice.

> ##### Holdius Prime
> ---
> A hive world with bustling cities and a terraformed landscape. Is responsible for the defense of the system and providing bodies for the Imperium.

> ##### Zhoroxria
> --- 
> A Death World with a constant overcast of green, toxic clouds. Its lush jungles are crawling with all manners of beasts and deadly insects. All life here is in a constant struggle. Is used as a recruiting world from different Space Marine Chapters, but none have a permanent hold on it. 

>##### Voidius
> ---
> A dark world, which in local lore is never talked about. No one knows what's going on behind the black clouds of Voidius. In later years, some have talked about seeing spore-like spaceships landing in the void. 

> ##### Twin worlds of Brunipius Alpha and Beta. 
> ---
> These two worlds are in a constant threat of colliding with each other, so civilization is sparse. B-Alpha is used a prison planet and B-Beta as a dumpster world, filled with junk and scraps. 
> 
> Orbiting these two bodies in an irregular pattern in the great moon Brunipius Zeta. First thought a planet of its own, after several centuries of debate, the ruling bodies of Holdius Eleven have decided it's a moon. After a smaller revolt, it was later dubbed "Great Moon". Several chapels of the Imperium containing both Adeptus Astartes representatives and Adepta Sorititas is stationed on the Great Moon.

##### The colorful rift
---
A strange small colorful rift has opened next to Holdius Prime and Blue H-1 2 centuries ago, shortly after the Great Rift opened. The local authorities sent request to Terra for investigations to be performed but help never came - until now. The different factions arriving to Holdius Eleven have to find out what the small rift means, and what other activities is going on in the system; why haven’t the two planets of Brunipius Alpha and Beta not collided yet? What's behind the black clouds of Voidius? How is the system faring after the Great Rift?
