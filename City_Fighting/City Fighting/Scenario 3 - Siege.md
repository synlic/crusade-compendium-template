# Scenario 3 - Siege
This scenario is played on the Hammer And Anvil Deployment map. Place no objective markers.

The defender places all the terrain on the table, with at least half of the terrain pieces being in their deployment zone. 

At the end of the game, the attacker scores:

*   10VP for each Terrain Piece in the defenders deployment zone that was destroyed or is embarked by a friendly unit. 

At the end of the game, the defender scores:

*   10VP for each Terrain Piece in the defenders deployment zone that was not destroyed.