# Scenario 1 - Destroy the command center
This scenario is played on the Search And Destroy Deployment map, but only place the objectives in No Mans Land.

Each player places 1 Medium Fortification & 1 Small Fortification in their deployment zone. These must be at least 6" away from each table edge and within 3" of each other. As long as the Small Fortification is alive, the Medium Fortification cannot be selected as targets for ranged attacks. 

Starting from the second battle round, at the end of each command phase, each player scores:

*   5VP if the control a objective marker
*   5VP if they control more objective markes than their opponent

At the end of the game, each players scores:

*   25VP if they destroyed the enemy's Small Fortification
*   50VP if they destroyed the enemy's Medium Fortification