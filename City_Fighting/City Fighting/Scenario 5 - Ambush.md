# Scenario 5 - Ambush
This scenario is played on the Dawn of War Deployment map, except both deployment zones are the attackers and No Mans Land is considers the defenders deployment zone and don't place any objective markers.

A straight road should be going through the whole defender deployment zone going from one short edge to another without going through the attackers deployment zone. Choose one short edge to be edge A, the other is edge B.

The Defender has to deploy their whole army within 24" of edge A and more then 9" from the attackers deployment zone before the attackers deploys. The defender cannot place any units in reserves (even during the battle). Units from the defenders army can only retreat if they are within 6" of edge B and more then 9" from the attackers deployment zone. 

At the end of the game, the attacker scores:

*   10VP for each enemy unit destroyed.

At the end of the game, the defender scores:

*   10VP for each friendly unit that retreated.