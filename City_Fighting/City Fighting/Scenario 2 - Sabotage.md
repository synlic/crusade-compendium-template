# Scenario 2 - Sabotage
This scenario is played on the Crucible of Battle Deployment map, but only place the objectives in No Mans Land.

Starting from the second battle round, at the end of each command phase, each player scores:

*   5VP if the control a objective marker
*   5VP if they control more objective markes than their opponent

At the end of the game, each players scores:

*   10VP for each Terrain Piece outside of the players deployment zone that was trapped and is not destroyed.