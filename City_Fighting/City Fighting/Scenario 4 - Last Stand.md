# Scenario 4 - Last Stand
This scenario is played on the  Hammer And Anvil Deployment map, except both deployment zones are the attackers and No Mans Land is considers the defenders deployment zone and don't place any objective markers.

The defender should place at least 4 terrain pieces within 16" of the center and 1 terrain piece on the center of the battlefield (the center piece should be a landing pad or some sort of evacuation zone). The defender can only deploy within 16" of the center of the battlefield. The defender cannot place any units in reserves (even during the battle). 

At the end of the game, the attacker scores:

*   100VP if less than 25% of the defenders army is within 9" of the center of the battlefield.

At the end of the game, the defender scores:

*   100VP if at least 25% of the defenders army is within 9" of the center of the battlefield.