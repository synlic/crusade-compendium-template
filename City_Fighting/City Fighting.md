# City Fighting
City Fightning
--------------

These are house rules for fightning in a city. It is recommended to fill the board with as many structures and ruins as possible.

After all the terrain has been placed on the board all players will have to agree to give one of the following special rules to the terrains features:

*   Large Ruin
*   Medium Ruin
*   Small Ruin
*   Large Fortification
*   Medium Fortification
*   Small Fortification
*   Trapped
*   Nothing special

Special Rules
-------------

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Name | Toughness | Wounds | Save | Special Rules |
| Large Ruin | 9   | 14  | 4+  | Embark 20, Firing Deck 15, Plunging Fire, Fortification |
| Medium Ruin | 9   | 10  | 4+  | Embark 12, Firing Deck 10, Plunging Fire, Fortification |
| Small Ruin | 9   | 8   | 4+  | Embark 7, Firing Deck 5, Plunging Fire, Fortification |
| Large Fortification | 12  | 18  | 2+  | Embark 20, Firing Deck 15, Deadly Demise D6, Plunging Fire, Fortification |
| Medium Fortification | 12  | 14  | 2+  | Embark 12, Firing Deck 10, Deadly Demise D3, Plunging Fire, Fortification |
| Small forticfication | 12  | 10  | 2+  | Embark 7, Firing Deck 5, Deadly Demise 1, Plunging Fire, Fortification |
| Trapped | N/A | N/A | N/A | Trapped |

### Terrains with wounds

If a terrain piece has wounds, it is counted as a neutral unit. If that terrain piece has friendly models embarked in it, it is counted as a friendly fortification. If that terrain piece has enemy models embarked in it, it is counted as a enemy fortification. 

A neutral terrain piece can be targeted by ranged attacks and abilities in the same way as a enemy model. A neutral terrain piece cannot be charged (except by vehicles, monsters and titanic units).

If a terrain piece with wounds is destroyed, remove that terrain piece from the table. If possible, add a crater or place a marker with a 6" aura that gives infantry units cover where that terrain piece was placed. 

### Embark X

A friendly or neutral terrain piece with Embark X, where X is it capacity in the number of slots is has, can be embarked and disembarked by infantry units in the same way as a regular friendly transport in your movement phase. A unit can embark in a terrain piece even if the disembarked earlier in the movement phase (a unit cannot however disembark from the same building they embarked within in the same phase). If this terrain piece is destroyed, embarked models roll to see if they take mortal wounds as usual with the exception that on a roll of 1 the unit takes 2 mortal wounds and on a 2 it takes 1 mortal wound. The only difference is that they can be placed in the same space as where the terrain piece was on the battlefield (they can still be placed wholly within 3" as usual).

Units may embark within a terrain piece during deployment if that piece is wholly within the players deployment zone. If that unit has infiltrate, you may also deploy it in terrain piece that is more than 9" away from the enemy's deployment zone. 

Some units take more than 1 slot in each terrain piece and units with the Fly keyword cannot embark (but they can charge it). All players agree before the battle begins, but here are some examples of units that takes 2 slots:

*   Terminator Armour
*   Gravis Armour
*   Ogres
*   Tyranid Warriors

If you charge a terrain piece that has enemy models inside of it, all the models in the charging units (except vehicles, monsters and titanic units) and the embarked models are considered of being in engagement range of each other. You have to touch the area of the terrain piece with at least one model to successfully finish your charge. Remove all the charging models from the table until the end of the fighting phase. At the end of the fighting phase; If all the embarked models are destroyed, the player who charged may embark models in it up to its capacity. Any units not embarked are placed wholly within 3" of the terrain piece. If any of the embarked models survive, all the charging units are placed wholly within 3" of the terrain piece with no models touching it. Neither the terrain piece, embarked models or the charging units are considered to be within engagement range any more. 

If you charge a terrain piece with a vehicle, monster or titanic unit you are counted as being in engagement range with only the terrain piece. Embarked models are not considered to be within engagement range with the charging units. You have to touch the area of the terrain piece with at least one model to successfully finish your charge. If that terrain piece is not destroyed, both the charging units and the terrain piece are considered to be in engagement range of each other (embarked units are not). 

### Firing Deck X

All embarked terrain pieces has the firing deck special rule. 

### Plunging Fire

When firing with the terrain piece using the Firing Deck special rule, all attacks are considered to have the Plunging Fire special rule. 

### Fortification

This terrain piece gains the Fortification keyword.

In addition; you can target units embarked in a terrain piece. If you do, worsen the shooting models BS by 1 and the embarked units gets the Cover special rule and can use the terrain piece's armour save. 

### Trapped

When a unit ends any move (except consolidate or pile-ins) within 1" of a terrain piece with the trapped special rule, roll 1D6. On 1 nothing happens. On a 2-4 that unit takes D3 Mortal Wounds. On a 5-6 that unit takes D3+3 Mortal Wounds. 

Trappers ignore all Mortal Wounds caused this way to that unit. 

### Trappers

Before the battle, each played select up to 2 infantry units with a OC of at least 1 to be trappers. These units gain the following special rule and gain the Trapper keyword:

In your shooting phase, if this unit has not performed any shooting attacks it can select a terrain piece within 1". That terrain piece gains the Trapped special rule. This unit is considered to have shot and cannot perform any charges this turn.