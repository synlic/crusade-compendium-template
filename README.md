
---
title: "Crusade Compendium Template"
author: [Synlic]
date: "2023-12-07"
keywords: [Warhammer, Crusade]
disable-header-and-footer: true
...

# Crusade Compendium Template

This is a repository used by me in my Crusade Campaings. Most notes are formated so they can be copy pasted as a note straight into administratum (https://administratum.goonhammer.com)