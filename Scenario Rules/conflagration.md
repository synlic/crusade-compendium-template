# Urban Battlezone: Conflagration

At the start of each battle round, add one Blaze token on each Ruin that already has a Blaze Token on it. Then the players roll one dice for each ruin on the battlefield that does not have a Blaze Token on it, adding 1 to the roll if there are ruins with a Blaze Token on it within 6"  of the ruin. On a 6+, add a Blaze Token on it. Then each player rolls for model within a ruin with a Blaze Token on it, adding 1 to the roll for each Blaze Token on the ruin. On a 7+, the models unit suffers 1 mortal wound. 

**Pillars of Smoke:** Models gaining cover from this ruin also gain the Stealth Keyword.

**Firefigters/Arsonists (Action):** At the end of your movement phase, one or more units from your army can perform this action if they are within 6" of a ruin. The action is completed at the end of your turn. For each unit that completes this action, you can either remove one Blaze Token (to a minimum of 1), or add one Blaze token (to a maxium of 6) on one ruin they are within 6" of. 