
---
title: "Crusade Compendium Template"
author: [Synlic]
date: "2022-03-28"
keywords: [Warhammer, Crusade]
disable-header-and-footer: true
...

# Crusade Compendium Template

## Joining the league
Write in the Discord Channel #Warzone-Nachmund and send the Campaign Master a copy of your list, either from Battlescribe (the file, not a copy paste) or from the official WH40k app. If you have a account on Administratum (https://administratum.goonhammer.com ) you can stead upload your army there and join the campaign via the code: CODE.

## Scheduling games
Either message in the discord channel or DM the person you want to challenge. See the section on "Campaign Phases" with rules on army sizes and Crusade Points and "Reporting Games" for how to report the results. 

## Campaign Phases
| Phase | Date | Power level | Minimum CrP | Maximum CrP|
|-------|------|-------------|-------------|------------|
| 0 | 25/4 - 30/4 | 25 | 0 | 5 |
| 1 | 1/5 - 14/5 | 50 | 5 | 10 |
| 2 | 15/5 - 4/6| - | 10 | 20 |
| 3 | 5/6 - 18/6 | - | 15 | 30 |

**Game day dates**

- 30th April
- 14th May
- 4th June
- 18th June

### Phase 0
You can play as much as you like, but games are limited to 25PL and a maximum of 5 Crusade Points.  The phase will end on the 30th April with a game day.

### Phase 1
All armies PL is increased to 50 PL for free. In each match the armies will always count as having at least 5 Crusade Points and can field a maximum of 10 Crusade Points.  The phase will end on the 14th may with a game day.

### Phase 2 and 3
No free increase in Power Level, but see the table above for limitations in CrP and game days. The campaign ends after the last game day and results will be posted on Discord. 

## War zone Points
War zone Points are the metric of victory and determine the winner of the league. Players are rewarded Warzone Points for each battle they complete. In the end there will be a winner alliance, player and faith.

You can see how many war zone points are awarded each game:

| - | Combat Patrol | Incursion | Strike Force | Onslaught | 
| ---| --- | --- | --- | --- | 
| Play a game | 1 | 1 | 1 | 1 |
| Draw a game | 1 | 2 | 3 | 4 |
| Win a game | 2 | 3 | 4 | 5 |

### War Zones
Each game will be in one of three different planets (War Zones) and in the end of every phase, the alliance with the highest war zone point in each war zone will move the token on step in their control. If they have more than double the points, it will move 2 steps. This will be tracked in Administratum. 


## Eligible rulebooks and materials
All rulebooks and supplements published by Games Workshop and Forgeworld are allowed for constructing an army. You are also allowed to use rules from White Dwarf that is under the "Warzone: NAME" sections. See also "Campaign Special Rules" section of this compendium for more special rules.

## Reporting games
### In Administratum
If you have a account in Administratum, just report the game there in the Campaign. Also send a message to the Campaign master what faiths was in the battle and what planet you were fighting on. 

### Using form
If you don't have access to Administratum, use this form:

```
Add link to here
```

## Winners!

### Alliance Winner
The alliance with the most held War Zones will be the winner.

### League Winner
The player who accrues the most Warzone Points throughout the league is crowned the League Warlord. 

# Campaign Special Rules

## Rule of Cool
You can at any time supersede any of the official and special rules if you and your opponent think it's really, really cool. If that means making a permanent change to a unit or the campaign, you have to consult the Campaign Master first. 

We are all here to have fun and create a awesome narrative, that is the most important part of this whole campaign. 

## Creating your Crusade force
Each player will create a new Crusade Force at the beginning of this league using the rules as written in the Warhammer 40k 9th edition Core Rules with the exceptions detailed in this compendium. If you have any questions about creating a crusade force, post them in the #channel of the community server and our community will be happy to help. 

### Upgrades and Battle Scars
When your units gains a upgrade or battle scar you choose from a table, you are not allowed to choose the upgrade but instead roll 2D6 and choose one of the results, re-rolling doubles and traits the unit already has. 

### Limits and restrictions on Crusade Points (CrP)
In each phase, there is a minimum and maximum amount of CrP you can field on the battlefield (not in your roster). See the table in "Campaign Phases" for the exact numbers.

This means that if you cannot field your army because you have too many Crusade Points you can always add as many Troops choices as you like to your army. These added units are only added to your army in that game and is not added to your roster. Units added this way cannot gain any experience or scars. They are able to score agendas and act as any other unit while in the battle, and you must field them in a valid Detachment as per usual rules. 

### Joining in a later phase
If you join a battle in phase 1 or later, your roster will start at 50PL and you gain the following bonuses:

| Phase | Requisition Bonus | # free upgrades to Battle-Ready | # free upgrades to Blooded |
|---|---|---|---|
| 1 | 1 | 3 | 0 |
| 2 | 4 | 4 | 3 |
| 3 | 8 | 5 | 5 |

All these bonuses are one time only and will not be available after you have created your Crusade Force. 

## Agenda and Victory Points
Every time you complete a agenda, you also gain 3 Victory Points. Each agenda can only give a maximum of 3 Victory Points in each battle round and 15 Victory Points in each battle

Examples of a Completed Agenda:

* You gain a tally when fulfilling a criteria explained in the agenda
* You fulfil a criteria at the end of the game.

## Permadeath
If a **character unit** gains more than two Battle Scars, it is permanently destroyed and must be removed from your Crusade Force roster. If that unit had any Relics or Crusade Relics, you roll a D6 for each fo them. On a 4+ that Relic or Crusade Relics can be given to another eligible unit (remember to increase the CrP accordingly), otherwise that Relic or Crusade Relic is considered lost. 

## Choosing the board size
When playing games in the Crusade League, be aware of how the board size will affect you and your opponent. Smaller board sizes favour melee armies, while larger board sizes favour those armies who are better at shooting or have good movement tricks. 

For Combat Patrol games, we recommend 44”x30” or 44”x44” boards. For Incursion games, we recommend 44”x44” or 60”x44” boards. Talk with your opponent before the game to decide what table size to play on so that the game will be fair and fun for both of you.

## Conceding and Retreating
An early concession can come at a cost to the winning player by denying them progress on experience, agendas, and other objectives. However, fighting a losing battle to the last man can also be detrimental, a poor narrative, and frankly unfun. To create gameplay opportunities in concession, the following rules apply:

- Retreat!: Starting on Battle Round 3, if every model in a unit is within 6” of a board edge at the end of their movement phase, you can remove that unit from the battlefield. The opposing player selects a unit on the battlefield to count as destroying the removed unit in all respects. Do not make an Out of Action test for any removed unit. 
- Total Slaughter: If a player concedes, then all of their units are destroyed and removed. The opposing player wins the game regardless of other victory conditions. For each unit removed, the winning player selects a unit on the battlefield to count as destroying the removed unit in all respects. The winner may continue to play the remaining Battle Rounds to finish any agendas or objectives. 

# Code of Conduct
## Casual play
While winning is fun, this league is about creating stories with personalized armies on gorgeous tables. Players are encouraged to bring their fluffiest armies and pet units, the kinds that haven’t been good since 4th edition but whose fluff got you into the game. If you’re only interested in creating the most tuned, competitive list, this league probably isn’t for you. 

\pagebreak

# Appendix 1 Rules from books

## Crusade Relics

## Requisitions

## Agendas

## Mission Pack

### Combat Patrol

### Incursion

### Strike Force

### Onslaught




