# OUT OF DATE
These rules are out of date and are replaced by the built in conquest module in Administratum. 

### :red[Territorial Rules]

> ##### :red[Achivements]
> ---
> * Most controlled territories (The Conqueror)
> * Most successful raids (The Marauder)
> * Most won games (The Warlord)
> * Most investigation points achieved (The Truthseeker)
> * Most Legendary Units (A new Legend)
> * Fewest wounded units of rank Battle-Hardened or higher (No one left behind)
> * Most wounded units of rank Battle-Hardened or higher (It's just a flesh wound)

> ##### :red[Winner]
> ---
> The player with the most achievements wins the campaign. 

> ##### :red[Terrestrial starting]
> ---
> * Your maximum RP is 7.
> * You can re-roll one failed out of action test after each game.
> * If you play a mission using the planet strike rules you must to be the defender if you're fighting in a territory you control.
> * You can control territories. 
> * You gain control of a territory if you win a battle in a territory that is adjacent to a territory you already controls, or is the first territory on a new system. 
> * Your crusade rules can also give you control over territories. 
> * You gain 1 RP for each territory you control after each phase.

> ##### :red[Space starting]
> ---
> * If you lose the battle, any units that survived gain 1 additional XP. 
> * If you play a mission using the planet strike rules you must to be the attacker.
> * You cannot control territories.
> * You perform a successful raid if you win a battle against a terrestrial starting army. 
> * If you win a battle in a territory your opponent controls, you gain 1 RP and your opponent loses 1 RP. 

> ##### :red[Investigations]
> * You gather investigation points (IP) through agendas.
> * The Game Master will provide lore and secrets through the campaign if you investigations points are high enough. 


#### :blue[Investigation Agendas]
---
> #### :grey[Shadow Operations]
> ---

> ##### :grey[Uncover the Answers]
> ---
> Keep an Uncover the Answers tally for each unit from your army. Add to a unit's tally each time it successfully completes the action:
>
> Uncover the Answers: One or more INFANTRY units can start to perform this action at the end of your Movement phase. Each unit from your army that starts to perform this action must be in range of a different objective marker that has not been uncovered. A unit cannot start this action while there are any enemy units (excluding AIRCRAFT) in range of the same objective marker. The action is completed at the end of your > turn. If this action is successfully completed, that objective marker is said to have been uncovered.
> 
> Each unit from your army gains 1 experience point for each mark on its Uncover the Answers tally.
> 
> At the end of the battle, if the total marks on the Uncover the Answers tallies of units in your army is equal to at least half the number of objective markers that were on the battlefield at the start of the battle, gain 1 Investigation point.

> ##### :grey[Make Contact]
> ---
> At the start of your first Command phase, select one objective marker on the battlefield that is not within your deployment zone to be a contact terminal. If there are no objective markers on the battlefield, then after both sides have finished deploying, your opponent must set up one objective marker on the battlefield that is not within their own deployment zone. This objective marker represents the contact terminal,  but does not count as an objective marker for any rules purpose other than for this Agenda.
> 
> Keep a Make Contact tally for each INFANTRY unit from your army. Add 1 to a unit's Make Contact tally each time it successfully completes the following action:
> 
> Make Contact: One INFANTRY unit from your army can start to perform this action at the end of your Movement phase if it is within range of your contact terminal objective marker and you control that objective marker. The action is completed at the end of your turn.
> 
> Each unit from your army gains a number of experience points equal to their Make Contact tally.
> 
> At the end of the battle, if any units from your army have three or more marks on their Make Contact tally, gain 1 Investigation point.

> #### :purple[Warpcraft]
> ---

> ##### :purple[Through the Stillness]
> ---
> Keep a tally for each PSYKER unit from your army. Each time a unit successfully completes the following psychic action, add 1 to that unit's tally:
> 
> Through the Stillness (Warp Charge 6): One PSYKER unit from your army can attempt to perform this psychic action during your Psychic phase if it is within 6" of a battlefield edge that has not been pierced by your army. If this psychic action is successfully completed, that battlefield edge is said to have been pierced by your army.
> 
> Each unit from your army gains a number of experience points equal to the marks on their Through the Stillness tally.
> 
> At the end of the battle, if 3 or more battlefield edges have been pierced by your army, gain 1 Investigation point.

> ##### :purple[Psychic Beacon]
> ---
> Keep a tally for each PSYKER unit from your army. Each time a unit successfully completes the following psychic action, add 1 to that unit's > tally:
> 
> Psychic Beacon (Warp Charge 5): One PSYKER unit from your army can attempt to perform this psychic action during your Psychic phase if it is within 6" of the centre of the battlefield. Each time a Psychic test is made for this psychic action, for each other friendly PSYKER unit that is within 6" of the centre of the battlefield, add 1 to that Psychic test.
> 
> Each unit from your army gains 1 experience point for each mark on its Psychic Beacon tally.
> 
> At the end of the battle, if the total marks on the Psychic Beacon tallies of all the units in your army is 3 or more, gain 1 Investigation > point.
